# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity

TEMPLATE = """
Keyval : {}
Keycode : {}
Accel Name　: {}
Accel Label : {}
"""


class DeltaViewer(Gtk.Label, DeltaEntity):

    def _on_key_released(self, event_controller_key, keyval, keycode, state):
        accel = Gtk.accelerator_get_label(keyval, state)
        shortcut = Gtk.accelerator_name_with_keycode(
            None,
            keyval,
            keycode,
            state
            )
        label = TEMPLATE.format(keyval, keycode, accel, shortcut)
        self.set_label(label)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(
            self,
            hexpand=True,
            vexpand=True,
            label="Press Any Keybind."
            )
        self.add_css_class("kusozako-primary-surface")
        controller = Gtk.EventControllerKey.new()
        controller.connect("key-released", self._on_key_released)
        application_window = self._enquiry("delta > application window")
        application_window.add_controller(controller)
        self._raise("delta > add to container", self)
