# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import gettext
import locale

VERSION = "2024.07.16"
APPLICATION_NAME = "kusozako-keycodes"
APPLICATION_ID = "com.gitlab.kusozako1.Keycodes"

locale.setlocale(locale.LC_ALL, None)
gettext.install(
    APPLICATION_NAME,
    "/usr/share/locale",
    names=('gettext', 'ngettext')
)

LONG_DESCRIPTION = "Keycode viwer for kusozako project."

COPYRIGHT = """Copyright © 2023
takeda.nemuru<takeda.nemuru@protonmail.com>"""

APPLICATION_DATA = {
    "version": VERSION,
    "min-files": 0,
    "max-files": 0,
    "application-name": APPLICATION_NAME,
    "rdnn-name": APPLICATION_ID,
    "application-id": APPLICATION_ID,
    "long-description": LONG_DESCRIPTION,
    "home-page": "https://www.gitlab.com/kusozako1/kusozako-keycodes",
    "copyright": COPYRIGHT,
    "license": "GPL-3.0-or-later",
}
