# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.main_loop.MainLoop import AlfaMainLoop
from . import APPLICATION_DATA
from .Viewer import DeltaViewer


class DeltaMainLoop(AlfaMainLoop):

    def _delta_call_loopback_application_window_ready(self, parent):
        DeltaViewer(parent)

    def _delta_info_data(self, key):
        return APPLICATION_DATA.get(key, None)
